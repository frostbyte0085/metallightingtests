//
//  Shaders.metal
//  BasicTexturing
//
//  Created by Warren Moore on 9/25/14.
//  Copyright (c) 2014 Metal By Example. All rights reserved.
//

#include <metal_stdlib>

using namespace metal;

struct Light
{
    float3 direction;
    float3 diffuseColor;
    float3 specularColor;
};

constant Light light = {
    .direction = { 0.13, 0.72, 0.68 },
    .diffuseColor = { 1, 1, 1 },
    .specularColor = { 0.5, 0.5, 0.5 }
};

struct Uniforms
{
    float4x4 modelViewProjectionMatrix;
    float4x4 modelViewMatrix;
    float3x3 normalMatrix;
};

struct Vertex
{
    float4 position [[attribute(0)]];
    float3 normal [[attribute(1)]];
    float2 texCoords [[attribute(2)]];
    float4 tangent [[attribute(3)]];
};

struct ProjectedVertex
{
    float4 position [[position]];
    float3 viewDir;
    float3 lightDir;
    float2 texCoords;
};

struct SkyboxVertex
{
    float4 position [[position]];
    float4 texCoords;
};

vertex SkyboxVertex vertex_skybox(device Vertex *vertices     [[buffer(0)]],
                                     constant Uniforms &uniforms [[buffer(1)]],
                                     uint vid                    [[vertex_id]])
{
    float4 position = vertices[vid].position;
    
    SkyboxVertex outVert;
    outVert.position = uniforms.modelViewProjectionMatrix * position;
    outVert.texCoords = position;
    return outVert;
}

fragment half4 fragment_skybox(SkyboxVertex vert          [[stage_in]],
                                    constant Uniforms &uniforms   [[buffer(0)]],
                                    texturecube<half> cubeTexture [[texture(0)]],
                                    sampler cubeSampler           [[sampler(0)]])
{
    float3 texCoords = float3(vert.texCoords.x, vert.texCoords.y, -vert.texCoords.z);
    return cubeTexture.sample(cubeSampler, texCoords);
}

vertex ProjectedVertex vertex_model(Vertex vert [[stage_in]],
                                   constant Uniforms &uniforms [[buffer(1)]])
{
    ProjectedVertex outVert;
    outVert.position = uniforms.modelViewProjectionMatrix * vert.position;
    outVert.texCoords = vert.texCoords;
    
    // tbn
    float3 normalEyeSpace = normalize(uniforms.normalMatrix * vert.normal);
    float3 tangentEyeSpace = normalize(uniforms.normalMatrix * vert.tangent.xyz);
    float3 bitangentEyeSpace = cross(normalEyeSpace, tangentEyeSpace) * vert.tangent.w;
    
    float3x3 tbn = float3x3( tangentEyeSpace, bitangentEyeSpace, normalEyeSpace );
    tbn = transpose(tbn);
    
    outVert.viewDir = tbn * -(uniforms.modelViewMatrix * vert.position).xyz;
    outVert.lightDir = tbn * light.direction;
    
    return outVert;
}

float3 F_Schlick(float3 F0, float3 V, float3 N)
{
    return F0 + (1.0 - F0) * pow(1 - saturate(dot(V, N)), 5);
}

float3 GGX(float3 N, float3 V, float3 L, float roughness, float3 F0)
{
    const float pi = 3.14159f;
    float alpha = roughness*roughness;
    
    float3 H = normalize(V+L);
    
    float dotNL = saturate(dot(N,L));
    float dotNH = saturate(dot(N,H));
    float dotLH = saturate(dot(L,H));
    
    float D, vis;
    float3 F;
    
    // D
    float alphaSqr = alpha*alpha;
    float denom = dotNH * dotNH *(alphaSqr-1.0) + 1.0f;
    D = alphaSqr/(pi * denom * denom);
    
    // F
    F = F_Schlick(F0, V, N);
    
    // V
    float k = alpha/2.0f;
    float k2 = k*k;
    float invK2 = 1.0 - k2;
    vis = 1.0 / (dotLH*dotLH*invK2 + k2);
    
    float3 specular = dotNL * D * F * vis;
    return specular * light.specularColor;
}

float3 Diffuse(float3 color, float3 N, float3 L)
{
    return light.diffuseColor * color * saturate(dot(N, L));
}

float3 Reflection(texturecube<float> envMap, sampler envSampler, float3 N, float3 V, float roughness, float3 F0)
{
    float3 F = F_Schlick(F0, V, N);
    float3 direction = reflect(V, N);

    // HACK
    // this is just a hack-approximation to compensate for the incorrectly generated cubemap.
    // ideally the cubemap mips should be generated based on the GGX implementation here, but since we don't have this
    // try to make the results a bit.. smoother.
    // /HACK
    float4 reflection = envMap.sample(envSampler, direction, level(pow(roughness, 0.5) * 10));
    
    return reflection.xyz * F;
}

float3 UnpackNormal(float4 in)
{
    return normalize(in.rgb * 2.0 - 1.0);
}

fragment float4 fragment_model(ProjectedVertex vert [[stage_in]],
                              constant Uniforms &uniforms [[buffer(0)]],
                              texture2d<float> albedoTexture [[texture(0)]],
                              texture2d<float> normalTexture [[texture(1)]],
                              texture2d<float> surfaceTexture [[texture(2)]],
                              texturecube<float> envTexture [[texture(3)]],
                              sampler defaultSampler [[sampler(0)]])
{
    float3 V = normalize(vert.viewDir);
    float3 L = normalize(vert.lightDir);
    float3 N = UnpackNormal(normalTexture.sample(defaultSampler, vert.texCoords.xy));
    float3 surfaceData = surfaceTexture.sample(defaultSampler, vert.texCoords.xy).rgb;
    float3 albedoColor = albedoTexture.sample(defaultSampler, vert.texCoords.xy).rgb;
    
    float occlusion = surfaceData.r;
    float roughness = 1.0 - surfaceData.g;
    float metalness = max(0.02, surfaceData.b);
    
    // save the metal color and scale the albedo based on the surface metalness
    float3 metalColor = albedoColor;
    albedoColor = mix(albedoColor, float3(0), metalness);
    
    // diffuse
    float3 diffuseTerm = Diffuse(albedoColor, N, L);
    
    // specular
    float3 specularColor = mix(metalness, metalColor*metalness, metalness);
    float3 specularTerm = GGX(N, V, L, roughness, specularColor);
    
    // reflection
    float3 reflectionTerm = Reflection(envTexture, defaultSampler, N, V, roughness, specularColor) * specularColor;
    
    // do the final composition
    float3 final = diffuseTerm;// * saturate(1.0 - specular);
    final += specularTerm;
    final += reflectionTerm;
    final *= occlusion;
    
    return float4(final, 1.0);
}
