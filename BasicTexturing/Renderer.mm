//
//  Renderer.m
//  BasicTexturing
//
//  Created by Warren Moore on 9/25/14.
//  Copyright (c) 2014 Metal By Example. All rights reserved.
//

#import "Renderer.h"
#import "Material.h"
#import "Mesh.h"
#import "Transformations.h"
#import "OBJGroup.h"
#import "SkyboxMesh.h"

#import <QuartzCore/CAMetalLayer.h>

@interface Renderer ()

@property (strong) UIView *view;
@property (weak) CAMetalLayer *layer;
@property (strong) id<MTLDevice> device;
@property (strong) id<MTLLibrary> library;
@property (strong) id<MTLRenderPipelineState> modelPipeline;
@property (strong) id<MTLRenderPipelineState> skyboxPipeline;
@property (strong) id<MTLCommandQueue> commandQueue;
@property (assign, getter=isPipelineDirty) BOOL pipelineDirty;
@property (strong) id<MTLBuffer> uniformBuffer;
@property (strong) id<MTLTexture> depthTexture;
@property (strong) id<MTLSamplerState> sampler;
@property (strong) MTLRenderPassDescriptor *currentRenderPass;
@property (strong) id<CAMetalDrawable> currentDrawable;
@property (assign) simd::float4x4 normalMatrix;
@property (strong) id<MTLTexture> envCubeTexture;
@property (nonatomic, strong) Mesh *skybox;

@end

@implementation Renderer

- (instancetype)initWithView:(UIView *)view
{
    if ((self = [super init]))
    {
        NSAssert([view.layer isKindOfClass:[CAMetalLayer class]], @"Layer type of view used for rendering must be CAMetalLayer");

        _view = view;
        _layer = (CAMetalLayer *)view.layer;
        _clearColor = [UIColor colorWithWhite:0.95 alpha:1];
        _pipelineDirty = YES;
        _device = MTLCreateSystemDefaultDevice();
        _layer.device = _device;
        _layer.pixelFormat = MTLPixelFormatBGRA8Unorm_sRGB;
        [self initializeDeviceDependentObjects];
    }
    
    return self;
}

- (void)initializeDeviceDependentObjects
{
    _library = [_device newDefaultLibrary];
    
    _commandQueue = [_device newCommandQueue];
    
    MTLSamplerDescriptor *samplerDescriptor = [MTLSamplerDescriptor new];
    samplerDescriptor.minFilter = MTLSamplerMinMagFilterLinear;
    samplerDescriptor.magFilter = MTLSamplerMinMagFilterLinear;
    samplerDescriptor.mipFilter = MTLSamplerMipFilterLinear;
    samplerDescriptor.maxAnisotropy = 8;
    _sampler = [_device newSamplerStateWithDescriptor:samplerDescriptor];
    
    NSArray *imageNames = @[@"px", @"nx", @"py", @"ny", @"pz", @"nz"];
    _envCubeTexture = [self textureCubeWithImagesNamed:imageNames];
    
    self.skybox = [[SkyboxMesh alloc] initWithDevice:self.device];
    
    _modelPipeline = [self configurePipelineForVertexFunctionNamed:@"vertex_model" andFragmentFunctionNamed:@"fragment_model"];
    _skyboxPipeline = [self configurePipelineForVertexFunctionNamed:@"vertex_skybox" andFragmentFunctionNamed:@"fragment_skybox"];
}

- (id<MTLRenderPipelineState>)configurePipelineForVertexFunctionNamed:(NSString*)vertexFunctionName andFragmentFunctionNamed:(NSString*)fragmentFunctionName
{
    MTLVertexDescriptor *vertexDescriptor = [MTLVertexDescriptor vertexDescriptor];
    vertexDescriptor.attributes[0].format = MTLVertexFormatFloat4;
    vertexDescriptor.attributes[0].bufferIndex = 0;
    vertexDescriptor.attributes[0].offset = offsetof(Vertex, position);
    
    vertexDescriptor.attributes[1].format = MTLVertexFormatFloat3;
    vertexDescriptor.attributes[1].bufferIndex = 0;
    vertexDescriptor.attributes[1].offset = offsetof(Vertex, normal);

    vertexDescriptor.attributes[2].format = MTLVertexFormatFloat2;
    vertexDescriptor.attributes[2].bufferIndex = 0;
    vertexDescriptor.attributes[2].offset = offsetof(Vertex, texCoords);
    
    vertexDescriptor.attributes[3].format = MTLVertexFormatFloat4;
    vertexDescriptor.attributes[3].bufferIndex = 0;
    vertexDescriptor.attributes[3].offset = offsetof(Vertex, tangent);

    vertexDescriptor.layouts[0].stride = sizeof(Vertex);
    vertexDescriptor.layouts[0].stepFunction = MTLVertexStepFunctionPerVertex;
    
    MTLRenderPipelineDescriptor *pipelineDescriptor = [MTLRenderPipelineDescriptor new];
    pipelineDescriptor.vertexFunction = [_library newFunctionWithName:vertexFunctionName];
    pipelineDescriptor.fragmentFunction = [_library newFunctionWithName:fragmentFunctionName];
    pipelineDescriptor.vertexDescriptor = vertexDescriptor;
    
    pipelineDescriptor.colorAttachments[0].pixelFormat = MTLPixelFormatBGRA8Unorm_sRGB;
    
    pipelineDescriptor.depthAttachmentPixelFormat = MTLPixelFormatDepth32Float;

    NSError *error = nil;
    id<MTLRenderPipelineState> pipeline = [self.device newRenderPipelineStateWithDescriptor:pipelineDescriptor
                                                                error:&error];
    
    if (!pipeline)
    {
        NSLog(@"Error occurred when creating render pipeline state: %@", error);
    }
    
    return pipeline;
}

- (uint8_t *)dataForImage:(UIImage *)image
{
    CGImageRef imageRef = [image CGImage];
    
    // Create a suitable bitmap context for extracting the bits of the image
    const NSUInteger width = CGImageGetWidth(imageRef);
    const NSUInteger height = CGImageGetHeight(imageRef);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    uint8_t *rawData = (uint8_t *)calloc(height * width * 4, sizeof(uint8_t));
    const NSUInteger bytesPerPixel = 4;
    const NSUInteger bytesPerRow = bytesPerPixel * width;
    const NSUInteger bitsPerComponent = 8;
    CGContextRef context = CGBitmapContextCreate(rawData, width, height,
                                                 bitsPerComponent, bytesPerRow, colorSpace,
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGColorSpaceRelease(colorSpace);
    
    // Flip the context so the positive Y axis points down
    CGContextTranslateCTM(context, 0, height);
    CGContextScaleCTM(context, 1, -1);
    
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    CGContextRelease(context);
    
    return rawData;
}

- (id<MTLTexture>)textureForImage:(NSString *)imageName andFormat:(MTLPixelFormat)format withMipmaps:(BOOL)mipmaps
{
    UIImage *image = [UIImage imageNamed:imageName];
    if (!image)
    {
        NSLog(@"Unable to find PNG image named \"%@\" in main bundle", imageName);
        return nil;
    }
    
    CGSize imageSize = CGSizeMake(image.size.width * image.scale, image.size.height * image.scale);
    const NSUInteger bytesPerPixel = 4;
    const NSUInteger bytesPerRow = bytesPerPixel * imageSize.width;
    uint8_t *imageData = [self dataForImage:image];
    
    MTLTextureDescriptor *textureDescriptor = [MTLTextureDescriptor texture2DDescriptorWithPixelFormat:format
                                                                                                 width:imageSize.width
                                                                                                height:imageSize.height
                                                                                             mipmapped:mipmaps];
    id<MTLTexture> texture = [_device newTextureWithDescriptor:textureDescriptor];
    
    MTLRegion region = MTLRegionMake2D(0, 0, imageSize.width, imageSize.height);
    [texture replaceRegion:region mipmapLevel:0 withBytes:imageData bytesPerRow:bytesPerRow];
    
    if (mipmaps)
    {
        // generate mipmaps
        id<MTLCommandBuffer> commandBuffer = [self.commandQueue commandBuffer];
        id<MTLBlitCommandEncoder> commandEncoder = [commandBuffer blitCommandEncoder];
        [commandEncoder generateMipmapsForTexture:texture];
        [commandEncoder endEncoding];
        [commandBuffer commit];
    }

    free(imageData);
    return texture;
}

- (id<MTLTexture>)textureCubeWithImagesNamed:(NSArray *)imageNameArray
{
    NSAssert(imageNameArray.count == 6, @"Cube texture can only be created from exactly six images");
    
    UIImage *firstImage = [UIImage imageNamed:[imageNameArray firstObject]];
    const CGFloat cubeSize = firstImage.size.width * firstImage.scale;
    
    const NSUInteger bytesPerPixel = 4;
    const NSUInteger bytesPerRow = bytesPerPixel * cubeSize;
    const NSUInteger bytesPerImage = bytesPerRow * cubeSize;
    
    MTLRegion region = MTLRegionMake2D(0, 0, cubeSize, cubeSize);
    
    MTLTextureDescriptor *textureDescriptor = [MTLTextureDescriptor textureCubeDescriptorWithPixelFormat:MTLPixelFormatRGBA8Unorm
                                                                                                    size:cubeSize
                                                                                               mipmapped:YES];
    
    id<MTLTexture> texture = [_device newTextureWithDescriptor:textureDescriptor];
    
    for (size_t slice = 0; slice < 6; ++slice)
    {
        NSString *imageName = imageNameArray[slice];
        UIImage *image = [UIImage imageNamed:imageName];
        uint8_t *imageData = [self dataForImage:image];
        
        NSAssert(image.size.width == cubeSize && image.size.height == cubeSize, @"Cube map images must be square and uniformly-sized");
        
        [texture replaceRegion:region
                   mipmapLevel:0
                         slice:slice
                     withBytes:imageData
                   bytesPerRow:bytesPerRow
                 bytesPerImage:bytesPerImage];
        
        free(imageData);
    }
    
    // generate mipmaps
    id<MTLCommandBuffer> commandBuffer = [self.commandQueue commandBuffer];
    id<MTLBlitCommandEncoder> commandEncoder = [commandBuffer blitCommandEncoder];
    [commandEncoder generateMipmapsForTexture:texture];
    [commandEncoder endEncoding];
    [commandBuffer commit];
    
    return texture;
}

- (Material *)newMaterialWithVertexFunctionNamed:(NSString *)vertexFunctionName
                           fragmentFunctionNamed:(NSString *)fragmentFunctionName
                             albedoTextureNamed:(NSString *)albedoTextureName
                             normalTextureNamed:(NSString *)normalTextureName
                             surfaceTextureNamed:(NSString *)surfaceTextureName
{
    id<MTLFunction> vertexFunction = [self.library newFunctionWithName:vertexFunctionName];
    
    if (!vertexFunction)
    {
        NSLog(@"Could not load vertex function named \"%@\" from default library", vertexFunctionName);
        return nil;
    }
    
    id<MTLFunction> fragmentFunction = [self.library newFunctionWithName:fragmentFunctionName];
    
    if (!fragmentFunction)
    {
        NSLog(@"Could not load fragment function named \"%@\" from default library", fragmentFunctionName);
        return nil;
    }
    
    id<MTLTexture> albedoTexture = [self textureForImage:albedoTextureName andFormat:MTLPixelFormatRGBA8Unorm_sRGB withMipmaps:YES];
    id<MTLTexture> normalTexture = [self textureForImage:normalTextureName andFormat:MTLPixelFormatRGBA8Unorm withMipmaps:YES];
    id<MTLTexture> surfaceTexture = [self textureForImage:surfaceTextureName andFormat:MTLPixelFormatRGBA8Unorm withMipmaps:YES];

    Material *material = [[Material alloc] initWithVertexFunction:vertexFunction
                                                 fragmentFunction:fragmentFunction
                                                   albedoTexture:albedoTexture
                                                   normalTexture:normalTexture
                                                   surfaceTexture:surfaceTexture];

    return material;
}

- (Mesh *)newMeshWithOBJGroup:(OBJGroup *)group
{
    id<MTLBuffer> vertexBuffer = [self.device newBufferWithBytes:group.vertexData.bytes
                                                          length:group.vertexData.length
                                                         options:MTLResourceOptionCPUCacheModeDefault];
    
    id<MTLBuffer> indexBuffer = [self.device newBufferWithBytes:group.indexData.bytes
                                                         length:group.indexData.length
                                                        options:MTLResourceOptionCPUCacheModeDefault];

    Mesh *mesh = [[Mesh alloc] initWithVertexBuffer:vertexBuffer indexBuffer:indexBuffer];
    return mesh;
}

- (void)createDepthBuffer
{
    CGSize drawableSize = self.layer.drawableSize;
    MTLTextureDescriptor *depthTexDesc = [MTLTextureDescriptor texture2DDescriptorWithPixelFormat:MTLPixelFormatDepth32Float
                                                                                            width:drawableSize.width
                                                                                           height:drawableSize.height
                                                                                        mipmapped:NO];
    self.depthTexture = [self.device newTextureWithDescriptor:depthTexDesc];
}

- (void)updateUniforms
{
    if (!self.uniformBuffer)
    {
        self.uniformBuffer = [self.device newBufferWithLength:sizeof(Uniforms)
                                                      options:MTLResourceOptionCPUCacheModeDefault];
    }

    Uniforms uniforms;
    uniforms.modelViewMatrix = self.modelViewMatrix;
    uniforms.modelViewProjectionMatrix = self.modelViewProjectionMatrix;
    uniforms.normalMatrix = simd::inverse(simd::transpose(UpperLeft3x3(self.modelViewMatrix)));
    
    memcpy([self.uniformBuffer contents], &uniforms, sizeof(Uniforms));
}

- (void)startFrame
{
    CGSize drawableSize = self.layer.drawableSize;

    if (!self.depthTexture || self.depthTexture.width != drawableSize.width || self.depthTexture.height != drawableSize.height)
    {
        [self createDepthBuffer];
    }
    
    MTLRenderPassDescriptor *renderPass = [MTLRenderPassDescriptor renderPassDescriptor];

    id<CAMetalDrawable> drawable = [self.layer nextDrawable];
    NSAssert(drawable != nil, @"Could not retrieve drawable from Metal layer");

    CGFloat r, g, b, a;
    [self.clearColor getRed:&r green:&g blue:&b alpha:&a];
    
    renderPass.colorAttachments[0].texture = drawable.texture;
    renderPass.colorAttachments[0].loadAction = MTLLoadActionClear;
    renderPass.colorAttachments[0].storeAction = MTLStoreActionStore;
    renderPass.colorAttachments[0].clearColor = MTLClearColorMake(r, g, b, a);

    renderPass.depthAttachment.texture = self.depthTexture;
    renderPass.depthAttachment.loadAction = MTLLoadActionClear;
    renderPass.depthAttachment.storeAction = MTLStoreActionStore;
    renderPass.depthAttachment.clearDepth = 1;

    self.currentDrawable = drawable;
    self.currentRenderPass = renderPass;
    
    [self updateUniforms];
}

- (void)drawSkybox
{
    id<MTLCommandBuffer> commandBuffer = [self.commandQueue commandBuffer];
    
    id<MTLRenderCommandEncoder> commandEncoder = [commandBuffer renderCommandEncoderWithDescriptor:self.currentRenderPass];
    
    MTLDepthStencilDescriptor *depthDescriptor = [MTLDepthStencilDescriptor new];
    depthDescriptor.depthCompareFunction = MTLCompareFunctionLess;
    depthDescriptor.depthWriteEnabled = NO;
    id <MTLDepthStencilState> depthState = [self.device newDepthStencilStateWithDescriptor:depthDescriptor];
    
    [commandEncoder setRenderPipelineState:self.skyboxPipeline];
    [commandEncoder setDepthStencilState:depthState];
    [commandEncoder setVertexBuffer:self.skybox.vertexBuffer offset:0 atIndex:0];
    [commandEncoder setVertexBuffer:self.uniformBuffer offset:0 atIndex:1];
    [commandEncoder setFragmentBuffer:self.uniformBuffer offset:0 atIndex:0];
    [commandEncoder setFragmentTexture:self.envCubeTexture atIndex:0];
    [commandEncoder setFragmentSamplerState:self.sampler atIndex:0];
    
    [commandEncoder drawIndexedPrimitives:MTLPrimitiveTypeTriangle
                               indexCount:[self.skybox.indexBuffer length] / sizeof(UInt16)
                                indexType:MTLIndexTypeUInt16
                              indexBuffer:self.skybox.indexBuffer
                        indexBufferOffset:0];
    
    [commandEncoder endEncoding];
    [commandBuffer commit];
}

- (void)drawMesh:(Mesh *)mesh withMaterial:(Material *)material
{
    id<MTLCommandBuffer> commandBuffer = [self.commandQueue commandBuffer];
    
    id<MTLRenderCommandEncoder> commandEncoder = [commandBuffer renderCommandEncoderWithDescriptor:self.currentRenderPass];

    [commandEncoder setVertexBuffer:mesh.vertexBuffer offset:0 atIndex:0];
    [commandEncoder setVertexBuffer:self.uniformBuffer offset:0 atIndex:1];
    [commandEncoder setFragmentBuffer:self.uniformBuffer offset:0 atIndex:0];
    [commandEncoder setFragmentTexture:material.albedoTexture atIndex:0];
    [commandEncoder setFragmentTexture:material.normalTexture atIndex:1];
    [commandEncoder setFragmentTexture:material.surfaceTexture atIndex:2];
    [commandEncoder setFragmentTexture:_envCubeTexture atIndex:3];
    [commandEncoder setFragmentSamplerState:self.sampler atIndex:0];
    
    [commandEncoder setRenderPipelineState:self.modelPipeline];
    [commandEncoder setCullMode:MTLCullModeBack];
    [commandEncoder setFrontFacingWinding:MTLWindingCounterClockwise];
    
    MTLDepthStencilDescriptor *depthStencilDescriptor = [MTLDepthStencilDescriptor new];
    depthStencilDescriptor.depthCompareFunction = MTLCompareFunctionLess;
    depthStencilDescriptor.depthWriteEnabled = YES;
    id<MTLDepthStencilState> depthStencilState = [self.device newDepthStencilStateWithDescriptor:depthStencilDescriptor];
    [commandEncoder setDepthStencilState:depthStencilState];

    [commandEncoder drawIndexedPrimitives:MTLPrimitiveTypeTriangle
                               indexCount:[mesh.indexBuffer length] / sizeof(UInt16)
                                indexType:MTLIndexTypeUInt16
                              indexBuffer:mesh.indexBuffer
                        indexBufferOffset:0];

    [commandEncoder endEncoding];
    [commandBuffer commit];
}

- (void)endFrame
{
    id<MTLCommandBuffer> commandBuffer = [self.commandQueue commandBuffer];
    [commandBuffer presentDrawable:self.currentDrawable];
    [commandBuffer commit];
}

@end
