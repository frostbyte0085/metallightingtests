//
//  SharedTypes.h
//  BasicTexturing
//
//  Created by Warren Moore on 9/25/14.
//  Copyright (c) 2014 Metal By Example. All rights reserved.
//

#ifndef BASICTEXTURING_SHAREDTYPES_H
#define BASICTEXTURING_SHAREDTYPES_H

#include <simd/simd.h>

struct Vertex
{
    simd::float4 position;
    simd::float3 normal;
    simd::float2 texCoords;
    simd::float4 tangent;
};

struct Uniforms
{
    simd::float4x4 modelViewProjectionMatrix;
    simd::float4x4 modelViewMatrix;
    simd::float3x3 normalMatrix;
};

typedef uint16_t IndexType;

#endif
