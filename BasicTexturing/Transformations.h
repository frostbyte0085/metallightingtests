//
//  Transformations.h
//  BasicTexturing
//
//  Created by Warren Moore on 9/27/14.
//  Copyright (c) 2014 Metal By Example. All rights reserved.
//

#import <simd/simd.h>

simd::float4x4 Identity();

simd::float4x4 Rotation(simd::float3 axis, float angle);

simd::float4x4 PerspectiveProjection(float aspect, float fovy, float near, float far);

simd::float3x3 UpperLeft3x3(const simd::float4x4 &mat);
