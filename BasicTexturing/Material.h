//
//  Material.h
//  BasicTexturing
//
//  Created by Warren Moore on 9/27/14.
//  Copyright (c) 2014 Metal By Example. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Metal/Metal.h>

@interface Material : NSObject

@property (strong) id<MTLFunction> vertexFunction;
@property (strong) id<MTLFunction> fragmentFunction;
@property (strong) id<MTLTexture> albedoTexture;
@property (strong) id<MTLTexture> normalTexture;
@property (strong) id<MTLTexture> surfaceTexture;

- (instancetype)initWithVertexFunction:(id<MTLFunction>)vertexFunction
                      fragmentFunction:(id<MTLFunction>)fragmentFunction
                        albedoTexture:(id<MTLTexture>)diffuseTexture
                        normalTexture:(id<MTLTexture>)normaltexture
                        surfaceTexture:(id<MTLTexture>)surfaceTexture;

@end
