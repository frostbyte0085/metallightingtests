//
//  MBESkyboxGeometry.h
//  MetalCubeMapping
//
//  Created by Warren Moore on 11/8/14.
//  Copyright (c) 2014 Metal By Example. All rights reserved.
//

#import "Mesh.h"

@interface SkyboxMesh : Mesh

- (instancetype)initWithDevice:(id<MTLDevice>) device;

@end
