//
//  OBJModel.mm
//  UpAndRunning3D
//
//  Created by Warren Moore on 9/11/14.
//  Copyright (c) 2014 Metal By Example. All rights reserved.
//

#import "OBJModel.h"
#import "OBJGroup.h"
#import "SharedTypes.h"

#include <map>
#include <vector>
#include <functional>

// "Face vertices" are tuples of indices into file-wide lists of positions, normals, and texture coordinates.
// We maintain a mapping from these triples to the indices they will eventually occupy in the group that
// is currently being constructed.
struct FaceVertex
{
    FaceVertex() :
        vi(0), ti(0), ni(0)
    {
    }
    
    uint16_t vi, ti, ni;
};

static bool operator <(const FaceVertex &v0, const FaceVertex &v1)
{
    if (v0.vi < v1.vi)
        return true;
    else if (v0.vi > v1.vi)
        return false;
    else if (v0.ti < v1.ti)
        return true;
    else if (v0.ti > v1.ti)
        return false;
    else if (v0.ni < v1.ni)
        return true;
    else if (v0.ni > v1.ni)
        return false;
    else
        return false;
}

@interface OBJModel ()
{
    std::vector<simd::float4> vertices;
    std::vector<simd::float3> normals;
    std::vector<simd::float2> texCoords;
    std::vector<Vertex> groupVertices;
    std::vector<IndexType> groupIndices;
    std::map<FaceVertex, IndexType> vertexToGroupIndexMap;
}

@property (nonatomic, strong) NSMutableArray *mutableGroups;
@property (nonatomic, weak) OBJGroup *currentGroup;
@property (nonatomic, assign) BOOL shouldGenerateNormals;

@end

@implementation OBJModel

- (instancetype)initWithContentsOfURL:(NSURL *)fileURL generateNormals:(BOOL)generateNormals
{
    if ((self = [super init]))
    {
        _shouldGenerateNormals = generateNormals;
        _mutableGroups = [NSMutableArray array];
        [self parseModelAtURL:fileURL];
    }
    return self;
}

- (NSArray *)groups
{
    return [_mutableGroups copy];
}

- (void)parseModelAtURL:(NSURL *)url
{
    NSError *error = nil;
    NSString *contents = [NSString stringWithContentsOfURL:url
                                                  encoding:NSASCIIStringEncoding
                                                     error:&error];
    if (!contents)
    {
        return;
    }
    
    NSScanner *scanner = [NSScanner scannerWithString:contents];
    
    NSCharacterSet *skipSet = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSCharacterSet *consumeSet = [skipSet invertedSet];
    
    scanner.charactersToBeSkipped = skipSet;
    
    NSCharacterSet *endlineCharacters = [NSCharacterSet newlineCharacterSet];
    
    [self beginGroupWithName:@"(unnamed)"];
    
    while (![scanner isAtEnd])
    {
        NSString *token = nil;
        if (![scanner scanCharactersFromSet:consumeSet intoString:&token])
        {
            break;
        }
        
        if ([token isEqualToString:@"v"])
        {
            float x, y, z;
            [scanner scanFloat:&x];
            [scanner scanFloat:&y];
            [scanner scanFloat:&z];
            
            simd::float4 v = { x, y, z, 1 };
            vertices.push_back(v);
        }
        else if ([token isEqualToString:@"vt"])
        {
            float u = 0, v = 0;
            [scanner scanFloat:&u];
            [scanner scanFloat:&v];
            
            simd::float2 vt = { u, v };
            texCoords.push_back(vt);
        }
        else if ([token isEqualToString:@"vn"])
        {
            float nx = 0, ny = 0, nz = 0;
            [scanner scanFloat:&nx];
            [scanner scanFloat:&ny];
            [scanner scanFloat:&nz];
            
            simd::float3 vn = { nx, ny, nz };
            normals.push_back(vn);
        }
        else if ([token isEqualToString:@"f"])
        {
            std::vector<FaceVertex> faceVertices;
            faceVertices.reserve(4);
            
            while (1)
            {
                int32_t vi = 0, ti = 0, ni = 0;
                if(![scanner scanInt:&vi])
                {
                    break;
                }

                if ([scanner scanString:@"/" intoString:NULL])
                {
                    [scanner scanInt:&ti];
                    
                    if ([scanner scanString:@"/" intoString:NULL])
                    {
                        [scanner scanInt:&ni];
                    }
                }
                
                FaceVertex faceVertex;
                
                // OBJ format allows relative vertex references in the form of negative indices, and
                // dictates that indices are 1-based. Below, we simultaneously fix up negative indices
                // and offset everything by -1 to allow 0-based indexing later on.
                
                faceVertex.vi = (vi < 0) ? (vertices.size() + vi - 1) : (vi - 1);
                faceVertex.ti = (ti < 0) ? (texCoords.size() + ti - 1) : (ti - 1);
                faceVertex.ni = (ni < 0) ? (vertices.size() + ni - 1) : (ni - 1);

                faceVertices.push_back(faceVertex);
            }
            
            [self addFaceWithFaceVertices:faceVertices];
        }
        else if ([token isEqualToString:@"g"])
        {
            NSString *groupName = nil;
            if ([scanner scanUpToCharactersFromSet:endlineCharacters intoString:&groupName])
            {
                [self beginGroupWithName:groupName];
            }
        }
    }
    
    [self endCurrentGroup];
}

- (void)beginGroupWithName:(NSString *)name
{
    [self endCurrentGroup];

    OBJGroup *newGroup = [[OBJGroup alloc] initWithName:name];
    [self.mutableGroups addObject:newGroup];
    self.currentGroup = newGroup;
}

- (void)endCurrentGroup
{
    if (!self.currentGroup)
    {
        return;
    }
    
    if (self.shouldGenerateNormals)
    {
        //[self generateNormalsForCurrentGroup];
    }
    
    [self generateTangentsForCurrentGroup];
    
    // Once we've read a complete group, we copy the packed vertices that have been referenced by the group
    // into the current group object. Because it's fairly uncommon to have cross-group shared vertices, this
    // essentially divides up the vertices into disjoint sets by group.

    NSData *vertexData = [NSData dataWithBytes:groupVertices.data() length:sizeof(Vertex) * groupVertices.size()];
    self.currentGroup.vertexData = vertexData;

    NSData *indexData = [NSData dataWithBytes:groupIndices.data() length:sizeof(IndexType) * groupIndices.size()];
    self.currentGroup.indexData = indexData;

    groupVertices.clear();
    groupIndices.clear();
    vertexToGroupIndexMap.clear();

    self.currentGroup = nil;
}

- (void)generateTangentsForCurrentGroup
{
    size_t indexCount = groupIndices.size();
    size_t vertexCount = groupVertices.size();
    
    simd::float3* tan1 = new simd::float3[vertexCount];
    simd::float3* tan2 = new simd::float3[vertexCount];
    
    for (size_t i=0; i<indexCount; ++i)
    {
        IndexType i1 = groupIndices[i + 0];
        IndexType i2 = groupIndices[i + 1];
        IndexType i3 = groupIndices[i + 2];
        
        Vertex v1 = groupVertices[i1];
        Vertex v2 = groupVertices[i2];
        Vertex v3 = groupVertices[i3];
        
        simd::float2 w1 = v1.texCoords;
        simd::float2 w2 = v2.texCoords;
        simd::float2 w3 = v3.texCoords;
        
        float x1 = v2.position.x - v1.position.x;
        float x2 = v3.position.x - v1.position.x;
        float y1 = v2.position.y - v1.position.y;
        float y2 = v3.position.y - v1.position.y;
        float z1 = v2.position.z - v1.position.z;
        float z2 = v3.position.z - v1.position.z;
        
        float s1 = w2.x - w1.x;
        float s2 = w3.x - w1.x;
        float t1 = w2.y - w1.y;
        float t2 = w3.y - w1.y;
        
        float div = (s1 * t2 - s2 * t1);
        float r = (fabs(div) > 0.000001f) ? 1.0f / div : 0.0f;
        
        simd::float3 sdir;
        sdir.x = (t2 * x1 - t1 * x2) * r;
        sdir.y = (t2 * y1 - t1 * y2) * r;
        sdir.z = (t2 * z1 - t1 * z2) * r;
        
        simd::float3 tdir;
        tdir.x = (s1 * x2 - s2 * x1) * r;
        tdir.y = (s1 * y2 - s2 * y1) * r;
        tdir.z = (s1 * z2 - s2 * z1) * r;
        
        tan1[i1] += sdir;
        tan1[i2] += sdir;
        tan1[i3] += sdir;
        
        tan2[i1] += tdir;
        tan2[i2] += tdir;
        tan2[i3] += tdir;
    }
    
    
    for (size_t i = 0; i < vertexCount; ++i)
    {
        Vertex& v = groupVertices[i];
        
        simd::float3 n = v.normal;
        simd::float3 t = tan1[i];

        [self orthoNormalizeNormal:n andTangent:t];
        v.tangent.x = t.x;
        v.tangent.y = t.y;
        v.tangent.z = t.z;
        
        v.tangent.w = (simd::dot(simd::cross(n, t), tan2[i]) < 0.0f) ? -1.0f : 1.0f;
    }
    
    delete[] tan1;
    delete[] tan2;
}

- (void)orthoNormalizeNormal:(simd::float3&)normal andTangent:(simd::float3&)tangent
{
    normal = simd::normalize(normal);
    simd::float3 proj = normal * simd::dot(tangent, normal);
    tangent -= proj;
    tangent = simd::normalize(tangent);
}

- (void)generateNormalsForCurrentGroup
{
    static const simd::float3 ZERO3 = { 0, 0, 0 };
    
    size_t i;
    size_t vertexCount = groupVertices.size();
    for (i = 0; i < vertexCount; ++i)
    {
        groupVertices[i].normal = ZERO3;
    }

    size_t indexCount = groupIndices.size();
    for (i = 0; i < indexCount; i += 3)
    {
        uint16_t i0 = groupIndices[i];
        uint16_t i1 = groupIndices[i + 1];
        uint16_t i2 = groupIndices[i + 2];
        
        Vertex *v0 = &groupVertices[i0];
        Vertex *v1 = &groupVertices[i1];
        Vertex *v2 = &groupVertices[i2];
        
        simd::float3 p0 = v0->position.xyz;
        simd::float3 p1 = v1->position.xyz;
        simd::float3 p2 = v2->position.xyz;
        
        simd::float3 cross = simd::cross((p1 - p0), (p2 - p0));

        v0->normal += cross;
        v1->normal += cross;
        v2->normal += cross;
    }

    for (i = 0; i < vertexCount; ++i)
    {
        groupVertices[i].normal = simd::normalize(groupVertices[i].normal);
    }
}

- (void)addFaceWithFaceVertices:(const std::vector<FaceVertex> &)faceVertices
{
    // Transform polygonal faces into "fans" of triangles, three vertices at a time
    for (size_t i = 0; i < faceVertices.size() - 2; ++i)
    {
        [self addVertexToCurrentGroup:faceVertices[0]];
        [self addVertexToCurrentGroup:faceVertices[i + 1]];
        [self addVertexToCurrentGroup:faceVertices[i + 2]];
    }
}

- (void)addVertexToCurrentGroup:(FaceVertex)fv
{
    static const simd::float3 UP = { 0, 1, 0 };
    static const simd::float2 ZERO2 = { 0, 0 };
    static const uint16_t INVALID_INDEX = 0xffff;
    
    uint16_t groupIndex;
    auto it = vertexToGroupIndexMap.find(fv);
    if (it != vertexToGroupIndexMap.end())
    {
        groupIndex = (*it).second;
    }
    else
    {
        Vertex vertex;
        vertex.position = vertices[fv.vi];
        vertex.normal = (fv.ni != INVALID_INDEX) ? normals[fv.ni] : UP;
        vertex.texCoords = (fv.ti != INVALID_INDEX) ? texCoords[fv.ti] : ZERO2;

        groupVertices.push_back(vertex);
        groupIndex = groupVertices.size() - 1;
        vertexToGroupIndexMap[fv] = groupIndex;
    }
    
    groupIndices.push_back(groupIndex);
}

@end
