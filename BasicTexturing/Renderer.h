//
//  Renderer.h
//  BasicTexturing
//
//  Created by Warren Moore on 9/25/14.
//  Copyright (c) 2014 Metal By Example. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Metal/Metal.h>
#import <simd/simd.h>
#import "SharedTypes.h"

@class Mesh, Material, OBJGroup;

@interface Renderer : NSObject

@property (strong) UIColor *clearColor;

@property (assign) simd::float4x4 modelViewProjectionMatrix;
@property (assign) simd::float4x4 modelViewMatrix;
@property (assign) simd::float3 cameraPosition;

- (instancetype)initWithView:(UIView *)view;

/// Creates a new material with the specified pair of vertex/fragment functions and
/// the specified diffuse texture name. The texture name must refer to a PNG resource
/// in the main bundle in order to be loaded successfully.
- (Material *)newMaterialWithVertexFunctionNamed:(NSString *)vertexFunctionName
                           fragmentFunctionNamed:(NSString *)fragmentFunctionName
                            albedoTextureNamed:(NSString *)albedoTextureName
                            normalTextureNamed:(NSString *)normalTextureName
                             surfaceTextureNamed:(NSString*)surfaceTextureName;

/// Creates a new mesh object from the specified OBJ group
- (Mesh *)newMeshWithOBJGroup:(OBJGroup *)group;

- (uint8_t *)dataForImage:(UIImage *)image;

- (void)startFrame;

- (void)drawMesh:(Mesh *)drawable withMaterial:(Material *)material;
- (void)drawSkybox;
- (void)endFrame;

@end
